package challenge

object Main {
    def main(args: Array[String]): Unit = {
        def verifyActions(args: Array[String]): Boolean = {
            val actions = args.drop(1).dropRight(1) :+ args(args.length-1).concat(",")
            !(actions.map(act => verifyAction(act)) contains false)
        }
        def verifyAction(act: String): Boolean = {
            import scala.util.matching.Regex
            val numberPattern: Regex = "([1-8],)".r
            (numberPattern findFirstMatchIn act).mkString("") == act
        }
        val verifyCommand: String = args match {
            case Array(x, _*) if !(x == "HOT" || x == "COLD") => "invalid temperature ... need HOT or COLD"
            case Array(x, _*) if verifyActions(args) && (x == "HOT" || x == "COLD") => "command valid"
            case Array(_) => "missing actions ... please add more arguments"
            case _ => "invalid actions [1-8] ... correct example HOT 8, 6, 4, 2, 1, 7"
        }
        // def dressingUp(args: Array[String]): String = {
        //     val acts: Array[String] = args.drop(1).map(act => act(0).toString())

        //     val hotDressing: String = acts match {
        //         case Array("8", "6", "4", "2", "1", "7") => "Removing PJS, shorts, shirt, sunglasses, sandals, leaving house, ...success"
        //         case Array("8", "6", "4", "2", "1", "7", _*) => "Removing PJS, shorts, shirt, sunglasses, sandals, leaving house, ...fail"
        //         case Array("8", "6", "4", "2", "1", _*) => "Removing PJS, shorts, shirt, sunglasses, sandals, fail"
        //         case Array("8", "6", "4", "2", _*) => "Removing PJS, shorts, shirt, sunglasses, fail"
        //         case Array("8", "6", "4", _*) => "Removing PJS, shorts, shirt, fail"
        //         case Array("8", "6", _*) => "Removing PJS, shorts, fail"
        //         case Array("8") => "Removing PJS, fail"
        //         case _ => "FAIL"
        //     }

        //     val coldDressing: String = acts match {
        //         case Array("8", "6", "3", "4", "2", "5", "1", "7") => "Removing PJS, pants, socks, shirt, hat, jacket, boots, leaving house, ...success"
        //         case Array("8", "6", "3", "4", "2", "5", "1", "7", _*) => "Removing PJS, pants, socks, shirt, hat, jacket, boots, leaving house, ...fail"
        //         case Array("8", "6", "3", "4", "2", "5", "1", _*) => "Removing PJS, pants, socks, shirt, hat, jacket, boots, fail"
        //         case Array("8", "6", "3", "4", "2", "5", _*) => "Removing PJS, pants, socks, shirt, hat, jacket, fail"
        //         case Array("8", "6", "3", "4", "2", _*) => "Removing PJS, pants, socks, shirt, hat, fail"
        //         case Array("8", "6", "3", "4", _*) => "Removing PJS, pants, socks, shirt, fail"
        //         case Array("8", "6", "3", _*) => "Removing PJS, pants, socks, fail"
        //         case Array("8", "6", _*) => "Removing PJS, pants, fail"
        //         case Array("8") => "Removing PJS, fail"
        //         case _ => "FAIL"
        //     }

        //     if(args(0) == "HOT")  hotDressing
        //     else if (args(0) == "COLD") coldDressing
        //     else ""
        // }

        case class Dress(result: String, options: Array[String])

        val initialOption = "Removing PJS"

        def dressing( dress: Dress, cloth: String, addition: String, repeat: Int = 1): Dress = {
            if( dress.options.count(o => o == cloth) >= repeat) {
                val result: String = if(cloth == initialOption) {
                    initialOption
                } else {
                    dress.result + ", " + cloth
                }
                val addArray: Array[String] = addition.split(",").map(_.trim)
                val option: Array[String] = addArray.foldLeft(dress.options.filter(d => d != cloth)) { (z, i) => 
                    z :+ i
                }
                Dress(result, option)
            } else {
                val result: String = dress.result + ", fail"
                Dress(result, Array(""))
            }
        }

        def dressingUp(args: Array[String]): String = {
            val acts: Array[String] = args.drop(1).map(act => act(0).toString())
            def hotDressing(act: String, dress: Dress): Dress = act match {
                case "8" => dressing( dress, "Removing PJS", "shorts, shirt")
                case "7" => dressing( dress, "leaving house", "", 2)
                case "6" => dressing( dress, "shorts", "sandals")
                case "4" => dressing( dress, "shirt", "sunglasses")
                case "2" => dressing( dress, "sunglasses", "leaving house")
                case "1" => dressing( dress, "sandals", "leaving house")
                case _ => dressing( dress, "", "")
            }
            def coldDressing(act: String, dress: Dress): Dress = act match {
                case "8" => dressing( dress, "Removing PJS", "pants, shirt")
                case "7" => dressing( dress, "leaving house", "", 3)
                case "6" => dressing( dress, "pants", "socks")
                case "5" => dressing( dress, "jacket", "leaving house")
                case "4" => dressing( dress, "shirt", "hat, jacket")
                case "3" => dressing( dress, "socks", "boots")
                case "2" => dressing( dress, "hat", "leaving house")
                case "1" => dressing( dress, "boots", "leaving house")
                case _ => dressing( dress, "", "")
            }
            val dressResult: String = 
            if(args(0) == "HOT") {
            (acts.foldLeft(Dress("", Array(initialOption))) { (z, act) => 
                hotDressing(act, z)
            }).result
            } else if(args(0) == "COLD") {
                (acts.foldLeft(Dress("", Array(initialOption))) { (z, act) =>
                coldDressing(act, z)
            }).result
            } else ""
            dressResult
        }

        def cleanUp(dressString: String): String = {
            if(dressString.contains("leaving house")){
                dressString.split("leaving house")(0) + "leaving house"
            } else if(dressString.contains("fail")){
                dressString.split("fail")(0) +  "fail"
            } else {
                dressString
            }
        }

        println("-" * 50)
        println(verifyCommand)
        println("-" * 50)
        if(verifyCommand == "command valid") println(cleanUp(dressingUp(args)))
    }
}